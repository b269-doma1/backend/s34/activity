const express = require("express");

// app - server
const app = express();

const port = 3000;

// Middlewares - software that provides common services and capabalities to application outside of what's offered by the operating system

// allows your app to read JSON data
app.use(express.json());

// allows your app to read data from any other forms
app.use(express.urlencoded({extended: true}));


// [SECTION] ROUTES
// GET Method
app.get("/greet", (request, response) => {
	response.send("Hello from the /greet endpoint!");
});

// POST Method
app.post("/hello", (request, response) => {
	response.send(`Hello there, ${request.body.firstName} ${request.body.lastName}!`);
});

// Simple registration form

let users = [];

app.post("/signup", (request, response) => {

	if(request.body.username !== '' && request.body.password !== '' ) {
		users.push(request.body);
		response.send(`User ${request.body.username} successfully rgistered!`);
	} else {
		response.send("Please input both username and password.");
	}
});

// Simple change password transaction

app.patch("/change-password", (request, response) => {

	let message;

	for(let i = 0; i < users.length; i++){
		if(request.body.username == users[i].username){
			users[i].password = request.body.passord;
			message = `User ${request.body.username}'s password has been updated!`;

			break;
		} else {
			message = "User does not exist."
		}
	}

	response.send(message);
});


// ======== ACTIVITY ==========

app.get("/home", (request, response) => {
	response.send("Welcome to the home page");
});

const items = [
  { 
  	username: "naruto",
   	password: "naruto123" 
   },

  { 
  	username: "johndoe", 
  	password: "johndoe123" 
  }
];

app.get('/items', (req, res) => {
  res.json(items);
});


app.delete('/delete-item', (req, res) => {
  const username = req.params.username;
  const index = users.findIndex(user => item.username === username);
  if (index !== -1) {
    items.splice(index, 1);
    res.send(`User ${username} has been deleted`);
  } else {
    res.send(`User ${username} not found`);
  }
});



app.listen(port, () => console.log(`Server running at ${port}`));


























